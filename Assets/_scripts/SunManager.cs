﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunManager : MonoBehaviour
{
    public List <DebrisEmmitor> debrisEmmitors = new List<DebrisEmmitor>();
    public Transform DebrisEmmitor_pivot;
    public float EmittorRotateSpeed = 5f;

    public GameObject DebrisEmmitor_pivot1;
    public GameObject DebrisEmmitor_pivot2;



    public Sun sun;

    private void Update()
    {

        DebrisEmmitor_pivot.transform.Rotate(new Vector3(0, EmittorRotateSpeed * Time.deltaTime, 0));

        if (sun.rage)
        {
            DebrisEmmitor_pivot1.SetActive(true);
            DebrisEmmitor_pivot2.SetActive(true);
            float r = Random.value;
            if (r >0.1f)
            {
                 debrisEmmitors[0].debrisDelay = 0.1f;
                debrisEmmitors[0].debrisSpeed = 5f;
            }
            else 
           {
                debrisEmmitors[0].debrisDelay = 0.05f;
                debrisEmmitors[0].debrisSpeed = 20f;
            }
            



        }
        else
        {
           DebrisEmmitor_pivot1.SetActive(false);
            DebrisEmmitor_pivot2.SetActive(false);
            debrisEmmitors[0].debrisDelay = 0.2f;
            debrisEmmitors[0].debrisSpeed = 5f;

        }
    }


}
