﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float smoothSpeed = 0.125f;

  

    // Update is called once per frame
    void FixedUpdate()
    {
        Quaternion desiredRot = GameManager.Instance.player_manager.player_controller.myPivot.localRotation;
        Quaternion smoothedRot = Quaternion.Lerp(transform.rotation, desiredRot, smoothSpeed*Time.deltaTime);
        transform.rotation = smoothedRot;

    }
}
