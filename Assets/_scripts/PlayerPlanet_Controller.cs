﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlanet_Controller : MonoBehaviour
{

    public Transform myPivot;
    public PlayerManager myManager;
    public float mouseSensitivity = 100f;
    float xRotation = 0f;
    bool isMovingRight = true;
    bool keyPressed = false;

    public float YearsSurvived = 0;

    private void Start()
    {
      //  Cursor.lockState = CursorLockMode.Locked;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.Instance.currentState == GameState.playing)
        {
            YearsSurvived += Time.deltaTime;
            int YS = (int)YearsSurvived;
            GameManager.Instance.uiManager.score.text = "Years Survived: "+YS.ToString();
        }
        

        if (Input.GetKey(KeyCode.A))
        {
            myPivot.transform.Rotate(new Vector3(0, myManager.player_gameplay.movementSpeed, 0));
            isMovingRight = false;
            keyPressed = true;
        }
         if (Input.GetKeyUp(KeyCode.A))
        {
            keyPressed = false;
        }
        if (Input.GetKey(KeyCode.D))
        {
            myPivot.transform.Rotate(new Vector3(0, -myManager.player_gameplay.movementSpeed, 0));
            isMovingRight = true;
            keyPressed = true;
        }
         if (Input.GetKeyUp(KeyCode.D))
        {
            keyPressed = false;
        }

        /*if (Input.GetKey(KeyCode.W))
        {
            if (myPivot.transform.localEulerAngles.z < 10f)
            {
                myPivot.transform.Rotate(new Vector3(0, 0, myManager.player_gameplay.movementSpeed));
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
                myPivot.transform.Rotate(new Vector3(0, 0, -myManager.player_gameplay.movementSpeed));
        }*/
       // myPivot.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Mathf.Clamp(transform.eulerAngles.z, 10f, -10f));




        if (!keyPressed)
        {
            if (isMovingRight)
            {
                myPivot.transform.Rotate(new Vector3(0, -myManager.player_gameplay.movementSpeed*0.3f, 0));
            }
            else
            {
                myPivot.transform.Rotate(new Vector3(0, myManager.player_gameplay.movementSpeed*0.3f, 0));
            }
        }

        //MoueseLook();
    }


    private void MoueseLook()
    {
        float MouseX = Input.GetAxis("Mouse X") * mouseSensitivity*Time.deltaTime;
        float MouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        //MouseY = Mathf.Clamp(MouseY, -35f, 60f);
        //transform.rotation = Quaternion.Euler(0, MouseX, 0);

        //xRotation -= MouseY;
        //xRotation = Mathf.Clamp(xRotation, -35f, 60f);
        //transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        transform.Rotate(Vector3.up * MouseX);

    }
}