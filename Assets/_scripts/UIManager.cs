﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class UIManager : MonoBehaviour
{
    public GameObject GameOverScreen;
    public TMP_Text GameoverDesc_text;
    public Slider healthSlider;
    public TMP_Text score;
    public TMP_Text current_coins;
    public TMP_Text totalCoins_text;

    public TMP_Text bullets;

    public GameObject MainMenu;


void Start()
{
        //PlayerPrefs.DeleteAll();

        MainMenu.SetActive(true);

        
        if (PlayerPrefs.HasKey("Total Coins"))
        {
            GameManager.Instance.totalCoins = PlayerPrefs.GetInt("Total Coins");
        }
        else
        {
            GameManager.Instance.totalCoins = 0;
        }
        totalCoins_text.text = "Total Coins "+ GameManager.Instance.totalCoins.ToString();

        if (PlayerPrefs.HasKey("Total Cost"))
        {
            Cost = PlayerPrefs.GetInt("Total Cost");
        }
        else
        {
            Cost = 10;
        }


    }
    public void UpdateHealth(float health)
    {
        GameManager.Instance.uiManager.healthSlider.value = health;
        if (health <=0)
        {
            EndGame();
            GameoverDesc_text.text = "Your planet was destroyed";
        }


    }

    public void TryAgain()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void Play()
    {
        GameManager.Instance.currentState = GameState.playing;
        GameManager.Instance.menu_audio.Stop();
        GameManager.Instance.main_audio.Play();

        MainMenu.SetActive(false);
    }

    public void EndGame()
    {
        GameManager.Instance.currentState = GameState.gameover;
        GameOverScreen.SetActive(true);


        float score = GameManager.Instance.player_manager.player_controller.YearsSurvived;
        int _coins = (int)score;
        
        current_coins.text = "Coins " + _coins.ToString();
        GameManager.Instance.totalCoins = PlayerPrefs.GetInt("Total Coins") ;
        GameManager.Instance.totalCoins += _coins;
        totalCoins_text.text = "Total Coins " + GameManager.Instance.totalCoins.ToString();

        PlayerPrefs.SetInt("Total Coins", GameManager.Instance.totalCoins);
        PlayerPrefs.Save();
        //GameoverDesc_text.text = "You took too much damage and got destroyed";

        Cost = PlayerPrefs.GetInt("Total Cost");
        Cost_txt.text = Cost.ToString() + "Coins";
    }

    public TMP_Text Amount_txt;
    public TMP_Text Cost_txt;
    public int Amount;
    public int Cost;

    public void Upgrade()
    {
        if (Cost <= GameManager.Instance.totalCoins)
        {
            Cost_txt.color = Color.black;

            GameManager.Instance.totalCoins -= Cost;
            totalCoins_text.text = "Total Coins " + GameManager.Instance.totalCoins.ToString();
            PlayerPrefs.SetInt("Total Coins", GameManager.Instance.totalCoins);
            PlayerPrefs.Save();

            GameManager.Instance.player_manager.player_gameplay.YellowBulletCount += 5;
            Amount_txt.text = GameManager.Instance.player_manager.player_gameplay.YellowBulletCount.ToString();
            PlayerPrefs.SetInt("Total Clips", GameManager.Instance.player_manager.player_gameplay.YellowBulletCount);
            PlayerPrefs.Save();

            

            Cost *= 3;
            Cost_txt.text = Cost.ToString() + "Coins";
            PlayerPrefs.SetInt("Total Cost", Cost);
            PlayerPrefs.Save();

        }
        else
        {
            Cost_txt.color = Color.red;
        }
    }


}
