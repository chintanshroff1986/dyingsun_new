﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum debrisColor {black, yellow, red};
public class Debris : MonoBehaviour
{
    
    public float currentTime = 0f;
    public debrisColor currentColor = debrisColor.black;
    public bool isGoingBack = false;

    private void OnEnable()
    {
        currentTime = 0f;
        float random = Random.Range(0.3f, 1.5f);
        transform.localScale = new Vector3(random, random,random);
        isGoingBack = false;
    }

    private void Update()
    {
        if (!isGoingBack)
        {
            MoveDebris();
        }
        else
        {
           transform.localPosition -= transform.forward * 25 * Time.deltaTime;
        }
        
    }

    public void MoveDebris()
    {

        // Smoothly move 
        //transform.localPosition = Vector3.Lerp(transform.localPosition, GameManager.Instance.player_manager.player_gameplay.transform.position, GameManager.Instance.sun_Manager.sun.debrisSpeed * Time.deltaTime);
        transform.localPosition += transform.forward * GameManager.Instance.sun_Manager.debrisEmmitors[0].debrisSpeed * Time.deltaTime;

        if (currentTime < GameManager.Instance.sun_Manager.debrisEmmitors[0].debris_lifeTime)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            Destroy();
        }
    }

    private void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.tag == "Player")
        {
            Destroy();
            if (currentColor == debrisColor.black)
            {
                GameManager.Instance.player_manager.player_gameplay.Health -= 0.1f;
                GameManager.Instance.uiManager.UpdateHealth(GameManager.Instance.player_manager.player_gameplay.Health);
                GameManager.Instance.camShake.shakeDuration = 0.5f;
            }
            else if (currentColor == debrisColor.yellow)
            {
                GameManager.Instance.player_manager.player_gameplay.UpdateBulllets(5);
            }
            else
            {
                GameManager.Instance.player_manager.player_gameplay.Health -= 0.15f;
                GameManager.Instance.uiManager.UpdateHealth(GameManager.Instance.player_manager.player_gameplay.Health);
                GameManager.Instance.camShake.shakeDuration = 0.5f;

            }
           
        }
        else if (other.gameObject.tag == "Bullet")
        {
            if (!other.gameObject.GetComponent<Bullet>().isYellowBullet)
            {
                Destroy();
                GameManager.Instance.player_manager.player_gameplay.UpdateBulllets(5);
            }
            if (currentColor == debrisColor.black)
            {
                isGoingBack = true;
            }
        }
        
        if (other.gameObject.tag == "Sun")
        {
            if (isGoingBack)
            {
                GameManager.Instance.sun_Manager.sun.IncreaseHealth();
                Destroy();
            }
        }
    }

    //on debris hit
    public void Destroy()
    {
        SimplePool.Despawn(this.gameObject);
    }
}
