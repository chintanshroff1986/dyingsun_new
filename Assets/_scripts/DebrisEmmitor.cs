﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisEmmitor : MonoBehaviour
{
    public Debris debris_black;
    public Debris debris_yellow;
    public Debris debris_red;


    public float debrisSpeed = 0.2f;
    public float debris_lifeTime = 100f;

    public float debrisDelay = 1f;
    public float timeSinceLastDebris = 0f;


    void SpawnDebris(float a, float b)
    {
        if (timeSinceLastDebris >= debrisDelay)
            {
                float r = Random.value;
                if (r >a)
                {
                    SimplePool.Spawn(debris_yellow.gameObject, transform.position, transform.rotation);
                }
                else if (r > b && r <=a)
                {
                    SimplePool.Spawn(debris_black.gameObject, transform.position, transform.rotation);
                }
                else
                {
                    SimplePool.Spawn(debris_red.gameObject, transform.position, transform.rotation);
                    
                }

               
                timeSinceLastDebris = 0;
            }
            else
            {
                timeSinceLastDebris += Time.deltaTime;
            }
    }

    private void Update()
    {
        if(GameManager.Instance.currentState == GameState.playing)
        {
            if (GameManager.Instance.sun_Manager.sun.rage)
            {
                SpawnDebris( 0.7f, 0.6f);

            }
            else
            {
                 SpawnDebris( 0.8f, 0.1f);
            }
        }
       
    }
}
