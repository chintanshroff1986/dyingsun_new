﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{

    public float sun_dyingSpeed = 2;
    public float maxSize = 8f;
    public float minSize = 1f;

    public bool rage = false;
    public float rageInterval = 15f;
    public float rageDuration = 8f;
    public float rageTimer = 0f;        





    // Update is called once per frame
    void Update()
    {


        if (GameManager.Instance.currentState == GameState.playing)
        {
            if (!rage)
            {
                ChangeSize();
                rageTimer +=Time.deltaTime;
                if (rageTimer >= rageInterval)
                {
                    rage = true;
                    rageTimer = 0;
                }
            
            }
            else
            {
                rageTimer +=Time.deltaTime;
                if (rageTimer >= rageDuration)
                {
                    rage = false;
                    rageTimer = 0;
                }
               
            }

            


             
        }
    }

    public void ChangeSize()
    {
        if (transform.localScale.y >= 1f)
            transform.localScale -= new Vector3(.1f, 0.1f, 0.1f) * Time.deltaTime * sun_dyingSpeed;
        else
        {
            GameManager.Instance.uiManager.healthSlider.value = 0;
            GameManager.Instance.player_manager.player_gameplay.Health = 0;

            GameManager.Instance.uiManager.EndGame();
        }
    }

    public void IncreaseHealth()
    {
         if (transform.localScale.x < maxSize)
        {
            transform.localScale += new Vector3(1f, 1f, 1f);
        }
        else
        {
            transform.localScale = new Vector3(maxSize, maxSize, maxSize);

        }
    }

    
}

