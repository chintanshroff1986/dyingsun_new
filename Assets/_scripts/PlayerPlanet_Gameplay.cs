﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlanet_Gameplay : MonoBehaviour
{
    public float movementSpeed = 1;
    public int bulletSpeed = 3;

    public Transform bulletSpawnPoint;
    public Bullet bullet_yellow;
    public Bullet bullet_red;


    public float Health = 100f;
    public int YellowBulletCount = 25;
   


    private void Start()
    {
        if (PlayerPrefs.HasKey("Total Clips"))
        {
            YellowBulletCount = PlayerPrefs.GetInt("Total Clips");

        }
        else
        {
            YellowBulletCount = 25;
            GameManager.Instance.uiManager.bullets.text = YellowBulletCount.ToString();
        }
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyUp(KeyCode.Space))
        {
            ShootYellow();
        }
        
    }


    void ShootYellow()
    {
        if (YellowBulletCount >0)
        {
           // Vector3 aimPos = GetMousePositionInPlaneOfLauncher();
            // aimPos.z = -1000;

            //Creating the bullet and shooting it
            Bullet b = SimplePool.Spawn(bullet_yellow.gameObject, bulletSpawnPoint.position, bulletSpawnPoint.rotation).GetComponent<Bullet>();
           // b.aimTarget = aimPos;

            //Playing the bullet noise
            //Shot.Play();
            UpdateBulllets(-1);


        }
       


    }
    void ShootRed()
    {
        Vector3 aimPos = GetMousePositionInPlaneOfLauncher();
        // aimPos.z = -1000;

        //Creating the bullet and shooting it
        Bullet b = SimplePool.Spawn(bullet_red.gameObject, bulletSpawnPoint.position, bulletSpawnPoint.rotation).GetComponent<Bullet>();
        b.aimTarget = aimPos;

        //Playing the bullet noise
        //Shot.Play();


    }

    public void UpdateBulllets(int b)
    {
        YellowBulletCount+= b;
        GameManager.Instance.uiManager.bullets.text = "Bullets " + YellowBulletCount.ToString();
    }


    Vector3 GetMousePositionInPlaneOfLauncher()
    {
        Plane p = new Plane(transform.right, transform.position);
        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        float d;
        if (p.Raycast(r, out d))
        {
            Vector3 v = r.GetPoint(d);
            return v;
        }

        throw new UnityException("Mouse position ray not intersecting launcher plane");
    }

}
